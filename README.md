# linuxcnc-flexi-HAL-3018

**Daniel Colon - 10-01-2023**

This is a config for running a generic chinese 3018 CNC router
(in this case a [Genmitsu 3018-PRO](https://www.sainsmart.com/products/sainsmart-genmitsu-cnc-router-3018-pro-diy-kit))
with a [Flexi-HAL](https://github.com/Expatria-Technologies/Flexi-HAL) board
using [LinuxCNC](http://linuxcnc.org/) with [remora-flexi-hal](https://github.com/Expatria-Technologies/remora-flexi-hal)
running off a Rapsberry Pi 4 with the image included in remora-flexi-hal.

This config is based on a the sample config included in remora-flexi-hal, but is
specific to the 3018 with added limit switches.

A custom board was made to drive the 3018s steppers with generic
[A4988](https://www.pololu.com/product/1182) stepper drivers configured for 1/8
microstepping.